package com.providerondemand.fargments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.providerondemand.R;
import com.providerondemand.adapter.CompleteOrderAdapter;
import com.providerondemand.adapter.PendingOrderAdapter;
import com.providerondemand.utilities.RecyclerClick;
import com.providerondemand.utilities.SpacesItemDecoration;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PendingOrderFragment extends Fragment implements RecyclerClick, SwipeRefreshLayout.OnRefreshListener  {

    RecyclerView pendingRv;
    SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView.LayoutManager mLayoutManager;
    PendingOrderAdapter pendingOrderAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_pending_order, container, false);
        //ButterKnife.bind(this, rootView);
        pendingRv = (RecyclerView) rootView.findViewById(R.id.pending_rv);
        mLayoutManager = new LinearLayoutManager(getActivity());
        swipeRefreshLayout=(SwipeRefreshLayout)rootView.findViewById(R.id.swipe_refresh);

        pendingOrderAdapter=new PendingOrderAdapter(getActivity(),this);
        mLayoutManager = new LinearLayoutManager(getActivity());
        pendingRv.setLayoutManager(mLayoutManager);
        pendingRv.setAdapter(pendingOrderAdapter);
        pendingOrderAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setOnRefreshListener(this);
        pendingRv.addItemDecoration(new SpacesItemDecoration(20));


        return rootView;
    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 100);
    }

    @Override
    public void productClick(View v, int position) {

    }
}
