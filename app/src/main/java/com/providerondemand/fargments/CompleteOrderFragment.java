package com.providerondemand.fargments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.providerondemand.R;
import com.providerondemand.adapter.CompleteOrderAdapter;
import com.providerondemand.utilities.RecyclerClick;
import com.providerondemand.utilities.SpacesItemDecoration;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CompleteOrderFragment extends Fragment implements RecyclerClick, SwipeRefreshLayout.OnRefreshListener {

    RecyclerView completeRv;

   // @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    private RecyclerView.LayoutManager mLayoutManager;
    CompleteOrderAdapter completeOrderAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_complete_order, container, false);
       // ButterKnife.bind(this, rootView);

        completeRv=(RecyclerView)rootView.findViewById(R.id.complete_rv);
        swipeRefresh=(SwipeRefreshLayout)rootView.findViewById(R.id.swipe_refresh);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(completeRv.getContext(), DividerItemDecoration.VERTICAL);
        completeOrderAdapter=new CompleteOrderAdapter(getActivity(),this);
        mLayoutManager = new LinearLayoutManager(getActivity());
        completeRv.setLayoutManager(mLayoutManager);
        completeRv.setAdapter(completeOrderAdapter);
        completeOrderAdapter.notifyDataSetChanged();
        swipeRefresh.setOnRefreshListener(this);
        completeRv.addItemDecoration(new SpacesItemDecoration(20));
        //completeRv.addItemDecoration(dividerItemDecoration);
        return rootView;
    }

    @Override
    public void productClick(View v, int position) {

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefresh.setRefreshing(false);
            }
        }, 100);
    }
}
