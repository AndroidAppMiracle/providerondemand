package com.providerondemand.fargments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.providerondemand.R;
import com.providerondemand.activities.MainActivity;

import butterknife.ButterKnife;

public class HomeFragment extends Fragment implements TabLayout.OnTabSelectedListener{

    ViewPager viewPager;

    public static HomeFragment newInstance() {

        HomeFragment fragment = new HomeFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, rootView);

        //((MainActivity) getActivity()).getmPrefs();
        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tabs_friends);
        tabLayout.addTab(tabLayout.newTab().setText("Pending Orders"));
        tabLayout.addTab(tabLayout.newTab().setText("Completed Order"));

        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager_friendsTab);

        viewPager.setAdapter(new HomeFragment.EventsAdapter(

                getChildFragmentManager(), tabLayout.getTabCount()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(0);
        tabLayout.setOnTabSelectedListener(this);
        return rootView;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    public class EventsAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public EventsAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {

                case 0:
                    fragment = new PendingOrderFragment();
                    return fragment;
                case 1:
                    fragment = new CompleteOrderFragment();
                    return fragment;
                default:
                    fragment = new PendingOrderFragment();
                    return fragment;

            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
