package com.providerondemand.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.providerondemand.R;
import com.providerondemand.utilities.RecyclerClick;

/**
 * Created by satoti.garg on 9/13/2017.
 */

public class PendingOrderAdapter  extends RecyclerView.Adapter<PendingOrderAdapter.ViewHolder>{


    Context mContext;
    RecyclerClick recyclerClick;


    public PendingOrderAdapter(Context mContext, RecyclerClick recyclerClick)
    {
        this.mContext = mContext;
        this.recyclerClick = recyclerClick;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pending_order_item, parent, false);
        return  new ViewHolder(itemView, recyclerClick);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView monthTv,dateTv,customerNameTv,orderNumberTv,servicesTv,pricetv;

        public ViewHolder(View view, RecyclerClick recyclerClick) {
            super(view);

            monthTv=(TextView)view.findViewById(R.id.month_tv);
            dateTv=(TextView)view.findViewById(R.id.date_tv);
            customerNameTv=(TextView)view.findViewById(R.id.name_customer_tv);
            orderNumberTv=(TextView)view.findViewById(R.id.order_number_tv);
            servicesTv=(TextView)view.findViewById(R.id.services_tv);
            pricetv=(TextView)view.findViewById(R.id.price_tv);
        }

    }
}
